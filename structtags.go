package optional

import (
	"reflect"
	"strconv"
	"strings"
)

//StructTagsOptionSource implements OptionSource, pulling data from Struct tags
type StructTagsOptionSource struct {
	o   *Opts
	key string
}

func getKeyStruct(k []string, v reflect.Type) (*reflect.StructField, bool) {
	if v.Kind() == reflect.Ptr {
		return getKeyStruct(k, v.Elem())
	}
	if v.Kind() == reflect.Interface {
		return getKeyStruct(k, v.Elem())
	}
	if v.Kind() != reflect.Struct {
		return nil, false
	}
	f, ok := v.FieldByName(k[0])
	if !ok {
		return nil, false
	}
	if len(k) == 1 {
		return &f, true
	}
	return getKeyStruct(k[1:], f.Type)
}

//Get implements OptionSource
func (s StructTagsOptionSource) Get(key string) (interface{}, bool) {
	ks := parse(key)
	k, ok := getKeyStruct(ks, s.o.i.Type())
	if !ok {
		return nil, false
	}
	v := structTagGet(k.Tag, s.key)
	if len(v) == 0 {
		return nil, false
	}
	return StrToType(v[0], k.Type), true
}

//StructTags creates a (StructTags)OptionSource pulling from struct's tags
func (o *Opts) StructTags(key string) StructTagsOptionSource {
	return StructTagsOptionSource{
		o:   o,
		key: key,
	}
}

//ArgumentsOptionSource implements OptionSource, pulling data from the passed argument array
type ArgumentsOptionSource struct {
	o *Opts
	s []string
}

//Arguments creates a (Arguments)OptionSource pulling data from the passed argument array.
//
// 	 Parser rules
//	 shorthand (1 char) has a single dash
//	 longhand (1+char ) has two dashes
//	 shorthand bools can be strung together -abcd will set a,b,c, and d to true
//	 shorthand bools with names begining with a '!' will be inverted ie: Fast bool `args:"Fast" args:"f" args:"!s"`
//	 '--' stops parsing
//
func (o *Opts) Arguments(s []string) ArgumentsOptionSource {
	return ArgumentsOptionSource{
		o: o,
		s: s,
	}
}

//Get implements OptionSource
func (s ArgumentsOptionSource) Get(key string) (interface{}, bool) {
	ks := parse(key)
	k, ok := getKeyStruct(ks, s.o.i.Type())
	if !ok {
		return nil, false
	}
	v := structTagGet(k.Tag, "arg")
	if len(v) == 0 {
		return nil, false
	}

	var shorthand []string
	var longhand []string

	for i := range v {
		if len(v[i]) == 1 {
			q := v[i]
			if q[0] == '!' {
				q = q[1:1] + "!"
			}
			shorthand = append(shorthand, q)
		} else {
			longhand = append(longhand, "--"+v[i])
		}
	}

	for i, arg := range s.s {
		if len(arg) >= 2 && arg[0:2] == "--" {
			if arg == "--" {
				break
			}
			for _, pargs := range longhand {
				if arg == pargs {
					if len(s.s) > i+1 && s.s[i+1][0] != '-' {
						return StrToType(s.s[i+1], k.Type), true
					} else if k.Type.Kind() == reflect.Bool {
						return true, true
					}
				}
			}
		}
	}

	for i, arg := range s.s {
		if arg == "--" {
			break
		}
		if len(arg) >= 2 && arg[0] == '-' && arg[1] != '-' {
			for _, parg := range shorthand {
				ind := strings.LastIndex(arg[1:], parg[:1])
				if ind != -1 {
					ind++
					if ind == len(arg)-1 && len(s.s) > i+1 && s.s[i+1][0] != '-' {
						return StrToType(s.s[i+1], k.Type), true
					} else if k.Type.Kind() == reflect.Bool {
						if len(parg) == 2 && parg[1] == '!' {
							return false, true
						}
						return true, true
					}
				}
			}
		}
	}
	return nil, false
}

//Modified reflect.StructTags.Get
func structTagGet(tag reflect.StructTag, key string) []string {
	r := []string{}
	for tag != "" {
		// skip leading space
		i := 0
		for i < len(tag) && tag[i] == ' ' {
			i++
		}
		tag = tag[i:]
		if tag == "" {
			break
		}

		// scan to colon.
		// a space or a quote is a syntax error
		i = 0
		for i < len(tag) && tag[i] != ' ' && tag[i] != ':' && tag[i] != '"' {
			i++
		}
		if i+1 >= len(tag) || tag[i] != ':' || tag[i+1] != '"' {
			break
		}
		name := string(tag[:i])
		tag = tag[i+1:]

		// scan quoted string to find value
		i = 1
		for i < len(tag) && tag[i] != '"' {
			if tag[i] == '\\' {
				i++
			}
			i++
		}
		if i >= len(tag) {
			break
		}
		qvalue := string(tag[:i+1])
		tag = tag[i+1:]

		if key == name {
			e, _ := strconv.Unquote(qvalue)
			r = append(r, e)
		}
	}
	return r
}

//StrToType converts a string to a type and puts it in a interface{}
func StrToType(str string, t reflect.Type) interface{} {
	switch t.Kind() {
	case reflect.Bool:
		switch strings.ToLower(str) {
		case "true", "t", "1", "y", "yes":
			return true
		default:
			return false
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		v, _ := strconv.ParseInt(str, 10, 64)
		return v
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		v, _ := strconv.ParseUint(str, 10, 64)
		return v
	case reflect.Float32, reflect.Float64:
		v, _ := strconv.ParseFloat(str, 64)
		return v
	default:
		return reflect.ValueOf(str).Convert(t).Interface()
	}
}
