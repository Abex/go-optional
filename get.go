package optional

import (
	"reflect"
	"sort"
	"strings"
)

func parse(v string) []string {
	return strings.Split(v, ".")[1:]
}

//Recalc sets all of the values in the struct to the correct values
//This is called automatically after AddSource
func (o *Opts) Recalc() {
	sSort := make([]int, len(o.sources))
	i := 0
	for k := range o.sources {
		sSort[i] = k
		i++
	}
	sort.Ints(sSort)
	o.sourcesSort = sSort
	recalc(o.i, "", o)
}

func recalc(v reflect.Value, k string, o *Opts) {
	switch v.Kind() {
	case reflect.Ptr:
		recalc(v.Elem(), k, o)
	case reflect.Interface:
		recalc(v.Elem(), k, o)
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			recalc(v.Field(i), k+"."+v.Type().Field(i).Name, o)
		}
	case reflect.Map:
		for _, key := range v.MapKeys() {
			recalc(v.MapIndex(key), k+"."+key.String(), o)
		}
	default:
		sourceIv, ok := o.Get(k)
		if ok {
			sourceV := reflect.ValueOf(sourceIv)
			v.Set(sourceV.Convert(v.Type()))
		}
	}
}

//Get will return the current value for a path
func (o *Opts) Get(key string) (interface{}, bool) {
	for i := len(o.sourcesSort) - 1; i >= 0; i-- {
		v, ok := o.sources[o.sourcesSort[i]].Get(key)
		if ok {
			return v, true
		}
	}
	return nil, false
}
