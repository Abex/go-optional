package optional

import "reflect"

//Wrapped wraps a struct or map implementing OptionSource
type Wrapped struct {
	V interface{}
}

//Get implements OptionSource
func (w Wrapped) Get(key string) (value interface{}, ok bool) {
	k := parse(key)
	return get(reflect.ValueOf(w.V), k)
}

func get(v reflect.Value, key []string) (value interface{}, ok bool) {
	if len(key) == 0 {
		return v.Interface(), true
	}
	switch v.Kind() {
	case reflect.Ptr:
		if !v.Elem().IsValid() {
			return nil, false
		}
		return get(v.Elem(), key)
	case reflect.Interface:
		if !v.Elem().IsValid() {
			return nil, false
		}
		return get(v.Elem(), key)
	case reflect.Struct:
		v = v.FieldByName(key[0])
		if !v.IsValid() {
			return nil, false
		}
		return get(v, key[1:])
	case reflect.Map:
		v = v.MapIndex(reflect.ValueOf(key[0]))
		if !v.IsValid() {
			return nil, false
		}
		return get(v, key[1:])
	default:
		if len(key) != 0 {
			return nil, false
		}
		return v.Interface(), true
	}
}
