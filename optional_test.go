package optional

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
)

func requireFailure(t *testing.T) {
	r := recover()
	if r == nil {
		t.Fail()
	}
}

type NewStructConfig struct {
	Whatever string
}

func TestNewStruct(t *testing.T) {
	s := NewStructConfig{}
	New(&s)
}

func TestNewNonPointer(t *testing.T) {
	s := NewStructConfig{}
	defer requireFailure(t)
	New(s)
}

func TestNewNonStruct(t *testing.T) {
	s := 5
	defer requireFailure(t)
	New(&s)
}

func TestWrapStruct(t *testing.T) {
	s := NewStructConfig{}
	o := New(&s)
	o.AddSource(0, Wrapped{NewStructConfig{
		Whatever: "success",
	}})
	if s.Whatever != "success" {
		t.Fail()
	}
}

func TestWrapMap(t *testing.T) {
	s := NewStructConfig{}
	o := New(&s)
	o.AddSource(0, Wrapped{map[string]string{
		"Whatever": "success",
	}})
	if s.Whatever != "success" {
		t.Fail()
	}
}

func TestCascading(t *testing.T) {
	s := NewStructConfig{}
	o := New(&s)
	o.AddSource(0, Wrapped{NewStructConfig{
		Whatever: "failure",
	}})
	o.AddSource(1, Wrapped{NewStructConfig{
		Whatever: "success",
	}})
	if s.Whatever != "success" {
		t.Fail()
	}
}

func TestCascadingSameIndex(t *testing.T) {
	s := NewStructConfig{}
	o := New(&s)
	o.AddSource(0, Wrapped{NewStructConfig{
		Whatever: "failure",
	}})
	defer requireFailure(t)
	o.AddSource(0, Wrapped{NewStructConfig{
		Whatever: "success",
	}})
}

type StructTags struct {
	Whatever  string `default:"success"`
	NoDefault string `somethingUnrelated:"default"`
}

func TestStructTags(t *testing.T) {
	s := StructTags{}
	o := New(&s)
	o.AddSource(0, Wrapped{map[string]string{
		"Whatever": "failure",
	}})
	o.AddSource(1, o.StructTags("default"))
	if s.Whatever != "success" {
		t.Fail()
	}
	if s.NoDefault != "" {
		t.Fail()
	}
}

type ArgShorthand struct {
	S string `arg:"s"`
	A bool   `arg:"a"`
	B bool   `arg:"b"`
	C bool   `arg:"!c"`
}

func TestArgShorthand(t *testing.T) {
	s := ArgShorthand{}
	o := New(&s)
	o.AddSource(1, o.Arguments(strings.Split("-abcs success", " ")))
	if !(s.A && s.B && !s.C && s.S == "success") {
		t.Fail()
	}
}

type ArgLonghand struct {
	S string `arg:"sa"`
	A bool   `arg:"aa"`
	B bool   `arg:"ba"`
	C bool   `arg:"ca"`
}

func TestArgLonghand(t *testing.T) {
	s := ArgLonghand{}
	o := New(&s)
	o.AddSource(1, o.Arguments(strings.Split("--aa --ba 1 --c 0 --sa success", " ")))
	if !(s.A && s.B && !s.C && s.S == "success") {
		t.Fail()
	}
}

type ArgDoubleDash struct {
	A bool `arg:"a"`
	B bool `arg:"bb"`
}

func TestArgDoubleDash(t *testing.T) {
	s := ArgDoubleDash{}
	o := New(&s)
	o.AddSource(1, o.Arguments(strings.Split("-- -a --bb", " ")))
	if s.A || s.B {
		t.Fail()
	}
}

type MultiLevelConfigTwo struct {
	A bool   `arg:"a2" default:"true"`
	B string `arg:"str2" default:"success"`
	C *MultiLevelConfig
}

type MultiLevelConfig struct {
	A bool `arg:"a" default:"true"`
	B *MultiLevelConfigTwo
}

func TestMultiLevelArg(t *testing.T) {
	s := MultiLevelConfig{
		B: &MultiLevelConfigTwo{},
	}
	o := New(&s)
	o.AddSource(1, o.Arguments(strings.Split("-a --a2 --str2 success", " ")))
	if !(s.A && s.B.A && s.B.B == "success") {
		t.Fail()
	}
}

func TestMultiLevelDefault(t *testing.T) {
	s := MultiLevelConfig{
		B: &MultiLevelConfigTwo{},
	}
	o := New(&s)
	o.AddSource(1, o.StructTags("default"))
	if !(s.A && s.B.A && s.B.B == "success") {
		t.Fail()
	}
}

func TestMultiLevelCopy(t *testing.T) {
	s := MultiLevelConfig{
		B: &MultiLevelConfigTwo{},
	}
	o := New(&s)
	o.AddSource(1, Wrapped{
		MultiLevelConfig{
			A: true,
			B: &MultiLevelConfigTwo{
				A: true,
				B: "success",
			},
		},
	})
	if !(s.A && s.B.A && s.B.B == "success") {
		t.Fail()
	}
}

func Example() {
	type Config struct {
		ConfigPath string `default:"/etc/test/config.json" arg:"c" arg:"conf-path"`
		Name       string `arg:"n" arg:"name" default:"New User"`
		Goodbye    bool   `default:"false" arg:"b" arg:"!h" arg:"goodbye"`
	}

	config := Config{}
	opts := New(&config)
	opts.AddSourceFast(0, opts.StructTags("default")) // Fast because we dont read until another AddSource
	opts.AddSource(2, opts.Arguments(
		[]string{"optional_test", "-n", "Jamie", "--conf-path", "c:/test.conf"},
	)) //normaly opts.AddSource(opts.Arguments(os.Args))

	c, err := ioutil.ReadFile(config.ConfigPath) //Open JSON on disk
	if err != nil {
		fmt.Println(err.Error())
	} else {
		m := make(map[string]interface{})
		json.Unmarshal(c, &m)
		opts.AddSource(1, Wrapped{m})
	}
	//Config should be set as:
	// ConfigPath: "D:/test.conf" (from Arguments)
	// Name:       "Jamie"        (from Arguments)
	// Goodbye:    false          (from Defaults)
	h := "Hello "
	if config.Goodbye {
		h = "Goodbye "
	}
	fmt.Println(h + config.Name)
	fmt.Println(config.ConfigPath)
	//Output will be: Hello Jamie (Unless you changed the conf)
}

func ExampleOpts_AddSourceFast() {
	type Config struct {
		A string `default:"default"`
		B string `default:"default" someOtherDefault:"OtherDefault"`
		C string `default:"default"`
	}

	config := Config{
		A: "Allocate", // Will be overwritten
	}
	opts := New(&config) // Does not change struct
	fmt.Println(config)
	//{Allocate  }

	opts.AddSource(1, opts.StructTags("default"))
	opts.AddSourceFast(2, opts.StructTags("someOtherDefault"))
	fmt.Println(config)
	//{default default default}
	opts.Recalc()
	fmt.Println(config)
	//{default OtherDefault default}

	//output:
	//{Allocate  }
	//{default default default}
	//{default OtherDefault default}
}
