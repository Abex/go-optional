// Copyright 2015 Abex
// This source is covered by the license in the LICENSE file, included with the source

/*
Package optional provides a cascading option resolver for cli applications

Usage:
Create a resolver by calling New(&config). Config should be a struct
Add sources by calling AddSource.
*/
package optional

import "reflect"

//Opts holds cascaded sources
type Opts struct {
	sources     map[int]OptionSource
	sourcesSort []int
	i           reflect.Value
}

//New creates a new Opts structure for the interface passed
func New(i interface{}) *Opts {
	ri := reflect.ValueOf(i)
	if ri.Kind() != reflect.Ptr {
		panic("You must use a Pointer")
	}
	ri = ri.Elem()
	if !ri.IsValid() {
		panic("Invalid pointer passed")
	}
	if ri.Kind() == reflect.Interface {
		ri = ri.Elem()
	}
	k := ri.Kind()
	if (k != reflect.Struct) && (k != reflect.Map) {
		panic("You must pass a pointer to a map or struct")
	}
	o := &Opts{
		sources: make(map[int]OptionSource),
		i:       ri,
	}
	return o
}

//A OptionSource can be passed to AddSource to add a source for values
type OptionSource interface {
	//Get will be given a path, period deliminated and should return the value related to it
	Get(key string) (value interface{}, ok bool)
}

//AddSource adds a source to the opt, 'i' being the priority level (higher number = higher priority)
func (o *Opts) AddSource(i int, s OptionSource) {
	if _, ok := o.sources[i]; ok {
		panic("Assigning source to existing source")
	}
	o.sources[i] = s
	o.Recalc()
}

//AddSourceFast adds a source to the opt without updating the parent struct
func (o *Opts) AddSourceFast(i int, s OptionSource) {
	if _, ok := o.sources[i]; ok {
		panic("Assigning source to existing source")
	}
	o.sources[i] = s
}
